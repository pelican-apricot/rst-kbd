=======================================================
``:kbd:`` reStructuredText text role plugin for Pelican
=======================================================


.. contents:: **Contents**
   :local:


Installation
============

1. Pointing to where this repository resides, for example

   .. code:: python

      PLUGIN_PATHS = ['/path/to/plugins']

2. Adding to plugin list

   .. code:: python

      PLUGINS = ['rst-kbd']


Usage
=====

In your reStructuredText source, you can write something like:

.. code:: rst

   Press :kbd:`Ctrl+C` to quit.


Contributing
============

You are welcome for any pull requests, but by opening a request, you agree to
place your contribution in the public domain and license under the UNLICENSE_
for wherever not applicable.

If you don't agree, please do not open a pull request.


Copyright
=========

The contents in this repository have been placed in the public domain, or via
UNLICENSE_, if not applicable.

.. _UNLICENSE: UNLICENSE
