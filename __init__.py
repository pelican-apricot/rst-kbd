# adding kbd reStructuredText text role for Pelican
#
# Written by Yu-Jie Lin in 2016, adopted from brc.py [1] (2013-2015)
#
# This plugin has been placed in the public domain, or via UNLICENSE,
# if not applicable.
#
# [1]: https://github.com/livibetter/dotfiles/blob/master/brc.py

from docutils import nodes
from docutils.parsers.rst import roles

__version__ = '0.1.0'
__author__ = 'Yu-Jie Lin'
__copyright__ = 'public domain'
__license__ = 'UNLICENSE'


def kbd(name, rawtext, text, lineno, inliner, options=None, content=None):
  '''Generate kbd element'''

  return [nodes.raw('', '<kbd>%s</kbd>' % text, format='html')], []


def register():

  roles.register_canonical_role('kbd', kbd)
